cov-build --dir idir go build

cov-analyze --dir idir --all --webapp-security --codexm ./do-not-call.cxm

# [optional] quick preview the 1st occurrence of the CALL_TO_Md5Sum defects in console
cov-format-errors --dir idir --emacs-style |grep "CALL_TO_Md5Sum"  -B 0 -A 4 -m 1

cov-commit-defects --dir idir --url https://poc05.coverity.synopsys.com --user username --password password --stream $app-name
